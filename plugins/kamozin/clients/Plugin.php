<?php namespace Kamozin\Clients;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            '\kamozin\clients\Components\Client'=>'client',

        ];
    }

    public function registerSettings()
    {
    }
}
