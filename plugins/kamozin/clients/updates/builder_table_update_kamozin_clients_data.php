<?php namespace Kamozin\Clients\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKamozinClientsData extends Migration
{
    public function up()
    {
        Schema::table('kamozin_clients_data', function($table)
        {
            $table->string('link');
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('kamozin_clients_data', function($table)
        {
            $table->dropColumn('link');
            $table->increments('id')->unsigned()->change();
        });
    }
}
