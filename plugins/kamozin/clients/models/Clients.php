<?php namespace Kamozin\Clients\Models;

use Model;

/**
 * Model
 */
class Clients extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kamozin_clients_data';

    public $attachOne=[
        'icon'=>'System\Models\File'
    ];


}