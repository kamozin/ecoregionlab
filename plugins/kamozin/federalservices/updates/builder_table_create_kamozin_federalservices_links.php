<?php namespace Kamozin\FederalServices\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKamozinFederalservicesLinks extends Migration
{
    public function up()
    {
        Schema::create('kamozin_federalservices_links', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->string('liks');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kamozin_federalservices_links');
    }
}
