<?php namespace Kamozin\FederalServices;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            '\kamozin\federalservices\Components\Links'=>'links',

        ];
    }

    public function registerSettings()
    {
    }
}
