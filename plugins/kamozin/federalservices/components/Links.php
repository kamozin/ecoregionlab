<?php namespace Kamozin\Federalservices\Components;

use Cms\Classes\ComponentBase;
use Kamozin\FederalServices\Models\FerationServices as federation;
class Links extends ComponentBase
{

    public $links;
    public function componentDetails()
    {
        return [
            'name'        => 'links Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }


    public function onRun() {

        $model=new federation();
        $this->links=$model->all();



    }
}
