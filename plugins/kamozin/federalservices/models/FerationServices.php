<?php namespace Kamozin\FederalServices\Models;

use Model;

/**
 * Model
 */
class FerationServices extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kamozin_federalservices_links';

    public $attachOne=[
        'icon'=>'System\Models\File'
    ];


}