<?php namespace Kamozin\Activities;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            '\kamozin\activities\Components\Activitie'=>'activitie',
            '\kamozin\activities\Components\ActivitieList'=>'activitielist',

        ];
    }

    public function registerSettings()
    {
    }
}
