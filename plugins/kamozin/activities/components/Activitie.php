<?php namespace Kamozin\Activities\Components;

use Cms\Classes\ComponentBase;
use Kamozin\Activities\Models\Activities as act;
class Activitie extends ComponentBase
{

    public $a;
    public $data;

    public function componentDetails()
    {
        return [
            'name'        => 'activitie Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }


    public function onRun(){

        $model=new act();

        $this->data=$model->all();

        $this->a=$model->where('slug', $this->param('slug'))->first();

    }
}
