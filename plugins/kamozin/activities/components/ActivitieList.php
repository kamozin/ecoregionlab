<?php namespace Kamozin\Activities\Components;

use Cms\Classes\ComponentBase;
use Kamozin\Activities\Models\Activities as act;
class ActivitieList extends ComponentBase
{
    public $data;
    public function componentDetails()
    {
        return [
            'name'        => 'activitieList Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){

        $model=new act();

        $this->data=$model->all();

    }
}
