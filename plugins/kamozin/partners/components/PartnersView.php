<?php namespace Kamozin\Partners\Components;

use Cms\Classes\ComponentBase;
use Kamozin\Partners\Models\Partners as partners;
class PartnersView extends ComponentBase
{
    public $partners;
    public function componentDetails()
    {
        return [
            'name'        => 'partners_view Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [

        ];
    }



    public function onRun(){

        $model=new partners();
        $this->partners=$model->all();

    }
}
