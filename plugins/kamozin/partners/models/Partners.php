<?php namespace Kamozin\Partners\Models;

use Model;

/**
 * Model
 */
class Partners extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kamozin_partners_data';
    public $attachOne=[
        'logo'=>'System\Models\File'
    ];
}