<?php namespace Kamozin\Partners;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return[
            '\kamozin\partners\Components\PartnersView' => 'partnersView'
        ];
    }

    public function registerSettings()
    {

    }
}
