<?php

/* D:\OpenServer\domains\eco.local/themes/demo/partials/site/footer.htm */
class __TwigTemplate_6c7f731b40a1811b78c010970449c5b679f1be3788714b8edf7e04b48b4f3f07 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer class=\"main-footer\">
    <div class=\"main-footer__inner container\">
        <div class=\"main-footer-logo\">
            <div class=\"main-footer-logo__image\">
                <img src=\"";
        // line 5
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("/assets/img/logo.png");
        echo "\" alt=\"\">
            </div>
            <div class=\"main-footer-logo__text\">
                <span class=\"main-footer-logo__name\">ЭКО РЕГИОН ЛАБ</span>
            </div>
        </div>
        <div class=\"main-footer-contacts\">
            <div class=\"main-footer-contacts__phone\">
                Телефон: <a href=\"tel:+7957896512\">+7 (4832) 40 00 39</a>
            </div>
            <div class=\"main-footer-contacts__phone\" style=\"font-size:0.78em;\">
                E-mail: <a href=\"mailto:info@eco-region-lab.pro\">info@eco-region-lab.pro</a>
            </div>

        </div>
        <div class=\"main-footer-socials\">
            <a class=\"main-footer-socials__item vk\" href=\"https://vk.com/club134396258 \" title=\"Вконтакте\">Вконтакте</a>
            <a class=\"main-footer-socials__item fb\" href=\"https://www.facebook.com/ecoregion32/\" title=\"Facebook\">Facebook</a>
            <a class=\"main-footer-socials__item ig\" href=\"https://www.instagram.com/eco_region_lab/ \" title=\"Instagram\">Instagram</a>
        </div>
        <div class=\"main-footer-developer\">
            <!--<div class=\"main-footer-developer__image\">-->
                <!--<img src=\"http://placehold.it/24x24\" alt=\"\"/>-->
            <!--</div>-->
            <!--<div class=\"main-footer-developer__text\">-->

            <!--</div>-->
        </div>
    </div>
</footer>";
    }

    public function getTemplateName()
    {
        return "D:\\OpenServer\\domains\\eco.local/themes/demo/partials/site/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 5,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<footer class=\"main-footer\">
    <div class=\"main-footer__inner container\">
        <div class=\"main-footer-logo\">
            <div class=\"main-footer-logo__image\">
                <img src=\"{{ '/assets/img/logo.png'|theme }}\" alt=\"\">
            </div>
            <div class=\"main-footer-logo__text\">
                <span class=\"main-footer-logo__name\">ЭКО РЕГИОН ЛАБ</span>
            </div>
        </div>
        <div class=\"main-footer-contacts\">
            <div class=\"main-footer-contacts__phone\">
                Телефон: <a href=\"tel:+7957896512\">+7 (4832) 40 00 39</a>
            </div>
            <div class=\"main-footer-contacts__phone\" style=\"font-size:0.78em;\">
                E-mail: <a href=\"mailto:info@eco-region-lab.pro\">info@eco-region-lab.pro</a>
            </div>

        </div>
        <div class=\"main-footer-socials\">
            <a class=\"main-footer-socials__item vk\" href=\"https://vk.com/club134396258 \" title=\"Вконтакте\">Вконтакте</a>
            <a class=\"main-footer-socials__item fb\" href=\"https://www.facebook.com/ecoregion32/\" title=\"Facebook\">Facebook</a>
            <a class=\"main-footer-socials__item ig\" href=\"https://www.instagram.com/eco_region_lab/ \" title=\"Instagram\">Instagram</a>
        </div>
        <div class=\"main-footer-developer\">
            <!--<div class=\"main-footer-developer__image\">-->
                <!--<img src=\"http://placehold.it/24x24\" alt=\"\"/>-->
            <!--</div>-->
            <!--<div class=\"main-footer-developer__text\">-->

            <!--</div>-->
        </div>
    </div>
</footer>", "D:\\OpenServer\\domains\\eco.local/themes/demo/partials/site/footer.htm", "");
    }
}
