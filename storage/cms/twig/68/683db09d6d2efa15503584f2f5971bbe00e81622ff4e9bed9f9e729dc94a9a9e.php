<?php

/* D:\OpenServer\domains\eco.local/themes/demo/layouts/default.htm */
class __TwigTemplate_734750540d476c3d1ad146b8df1b5f6d56351365c3f924b496fb36b687b84080 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    <title>Эко-Регион-Лаб - ";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "title", array()), "html", null, true);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "meta_description", array()), "html", null, true);
        echo "\">
    <meta name=\"title\" content=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "meta_title", array()), "html", null, true);
        echo "\">
    <meta name=\"author\" content=\"SiteWeb\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta name=\"generator\" content=\"\">
    <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 11
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/october.png");
        echo "\">
    <link href=\"";
        // line 12
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/main.css");
        echo "\" rel=\"stylesheet\">
    ";
        // line 13
        echo $this->env->getExtension('CMS')->assetsFunction('css');
        echo $this->env->getExtension('CMS')->displayBlock('styles');
        // line 14
        echo "</head>

<body>

";
        // line 18
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('CMS')->partialFunction("site/header"        , $context['__cms_partial_params']        );
        unset($context['__cms_partial_params']);
        // line 19
        echo "
";
        // line 20
        echo $this->env->getExtension('CMS')->pageFunction();
        // line 21
        echo "

";
        // line 23
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('CMS')->partialFunction("site/footer"        , $context['__cms_partial_params']        );
        unset($context['__cms_partial_params']);
        // line 24
        echo "<div class=\"popup popup-callback mfp-hide\" id=\"popup-callback\">
    <div class=\"popup__title\">Обратный звонок</div>
    <form class=\"form-callback\" name=\"form-callback\" data-request=\"onSendCallback\" data-request-success=\"ok(data)\" action=\"\" method=\"post\">
        <div class=\"form-item\">
            <label class=\"form-label form-label--required\" for=\"field-callback-name\">Представьтесь, пожалуйста</label>
            <input class=\"form-control\" type=\"text\" id=\"field-callback-name\" name=\"callback-name\" placeholder=\"\"
                   required>
        </div>
        <div class=\"form-item\">
            <label class=\"form-label form-label--required\" for=\"field-callback-tel\">Номер телефона</label>
            <input class=\"form-control\" type=\"tel\" id=\"field-callback-tel\" name=\"callback-tel\" placeholder=\"\" required>
        </div>
        <div class=\"form-submit-wrapper\">
            <button class=\"form-submit button\" type=\"submit\">Отправить</button>
        </div>
    </form>
</div>

<!--  Форма вопроса-->
<div class=\"popup popup-question mfp-hide\" id=\"popup-question\">
    <div class=\"popup__title\">Задать вопрос</div>
    <form class=\"form-question\" name=\"form-question\" data-request=\"onSendQuestion\" data-request-success=\"ok(data)\"  action=\"\" method=\"post\">
        <label class=\"form-item\">
            <span class=\"form-label form-label--required\">Представьтесь, пожалуйста</span>
            <input class=\"form-control\" type=\"text\" id=\"question-name\" name=\"question-name\" placeholder=\"\" required>
        </label>
        <label class=\"form-item\">
            <span class=\"form-label form-label--required\">E-mail</span>
            <input class=\"form-control\" type=\"email\" id=\"question-email\" name=\"question-email\" placeholder=\"\" required>
        </label>
        <label class=\"form-item\">
            <span class=\"form-label form-label--required\">Сообщение:</span>
            <textarea class=\"form-control\" id=\"question-message\" name=\"question-message\" rows=\"6\" required></textarea>
        </label>

        <div class=\"form-submit-wrapper\">
            <button class=\"form-submit button\" type=\"submit\">Отправить</button>
        </div>
    </form>
</div>

<div class=\"popup popup-callback mfp-hide\" id=\"popup-thanks\">
    <div class=\"popup__title\">Спасибо за обращение</div>
</div>

<!-- Scripts -->

<script src=\"https://code.jquery.com/jquery-2.2.4.min.js\"></script>
<script src=\"";
        // line 72
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/js/jquery.magnific-popup.min.js");
        echo "\"></script>
<script src=\"";
        // line 73
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/js/script.js");
        echo "\"></script>
<script src=\"";
        // line 74
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/vendor/bootstrap.js");
        echo "\"></script>
<script src=\"";
        // line 75
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/javascript/app.js");
        echo "\"></script>
";
        // line 76
        echo '<script src="'. Request::getBasePath()
                .'/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
        echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        echo '<link rel="stylesheet" property="stylesheet" href="'. Request::getBasePath()
                    .'/modules/system/assets/css/framework.extras.css">'.PHP_EOL;
        // line 77
        echo $this->env->getExtension('CMS')->assetsFunction('js');
        echo $this->env->getExtension('CMS')->displayBlock('scripts');
        // line 78
        echo "
<script>

    if(window.location.pathname=='/'){

        \$('body').addClass('homepage');
    }
</script>
<!-- Yandex.Metrika counter -->
<script type=\"text/javascript\">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter41686964 = new Ya.Metrika({
                    id: 41686964,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName(\"script\")[0],
                s = d.createElement(\"script\"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
        s.type = \"text/javascript\";
        s.async = true;
        s.src = \"https://mc.yandex.ru/metrika/watch.js\";

        if (w.opera == \"[object Opera]\") {
            d.addEventListener(\"DOMContentLoaded\", f, false);
        } else {
            f();
        }
    })(document, window, \"yandex_metrika_callbacks\");
</script>


<script>
    
    function ok (data){
        if (data.result=='ok'){
            \$('#popup-thanks').modal('show');
            \$.magnificPopup.open({
              items: {
                src: '#popup-thanks'
              },
              type: 'inline'

            }, 0);
        }
    }
</script>

<noscript>
    <div><img src=\"https://mc.yandex.ru/watch/41686964\" style=\"position:absolute; left:-9999px;\" alt=\"\"/></div>
</noscript>
<!-- /Yandex.Metrika counter -->


</body>
</html>";
    }

    public function getTemplateName()
    {
        return "D:\\OpenServer\\domains\\eco.local/themes/demo/layouts/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 78,  147 => 77,  140 => 76,  136 => 75,  132 => 74,  128 => 73,  124 => 72,  74 => 24,  70 => 23,  66 => 21,  64 => 20,  61 => 19,  57 => 18,  51 => 14,  48 => 13,  44 => 12,  40 => 11,  33 => 7,  29 => 6,  25 => 5,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    <title>Эко-Регион-Лаб - {{ this.page.title }}</title>
    <meta name=\"description\" content=\"{{ this.page.meta_description }}\">
    <meta name=\"title\" content=\"{{ this.page.meta_title }}\">
    <meta name=\"author\" content=\"SiteWeb\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta name=\"generator\" content=\"\">
    <link rel=\"icon\" type=\"image/png\" href=\"{{ 'assets/images/october.png'|theme }}\">
    <link href=\"{{ 'assets/css/main.css'|theme }}\" rel=\"stylesheet\">
    {% styles %}
</head>

<body>

{% partial \"site/header\" %}

{% page %}


{% partial \"site/footer\" %}
<div class=\"popup popup-callback mfp-hide\" id=\"popup-callback\">
    <div class=\"popup__title\">Обратный звонок</div>
    <form class=\"form-callback\" name=\"form-callback\" data-request=\"onSendCallback\" data-request-success=\"ok(data)\" action=\"\" method=\"post\">
        <div class=\"form-item\">
            <label class=\"form-label form-label--required\" for=\"field-callback-name\">Представьтесь, пожалуйста</label>
            <input class=\"form-control\" type=\"text\" id=\"field-callback-name\" name=\"callback-name\" placeholder=\"\"
                   required>
        </div>
        <div class=\"form-item\">
            <label class=\"form-label form-label--required\" for=\"field-callback-tel\">Номер телефона</label>
            <input class=\"form-control\" type=\"tel\" id=\"field-callback-tel\" name=\"callback-tel\" placeholder=\"\" required>
        </div>
        <div class=\"form-submit-wrapper\">
            <button class=\"form-submit button\" type=\"submit\">Отправить</button>
        </div>
    </form>
</div>

<!--  Форма вопроса-->
<div class=\"popup popup-question mfp-hide\" id=\"popup-question\">
    <div class=\"popup__title\">Задать вопрос</div>
    <form class=\"form-question\" name=\"form-question\" data-request=\"onSendQuestion\" data-request-success=\"ok(data)\"  action=\"\" method=\"post\">
        <label class=\"form-item\">
            <span class=\"form-label form-label--required\">Представьтесь, пожалуйста</span>
            <input class=\"form-control\" type=\"text\" id=\"question-name\" name=\"question-name\" placeholder=\"\" required>
        </label>
        <label class=\"form-item\">
            <span class=\"form-label form-label--required\">E-mail</span>
            <input class=\"form-control\" type=\"email\" id=\"question-email\" name=\"question-email\" placeholder=\"\" required>
        </label>
        <label class=\"form-item\">
            <span class=\"form-label form-label--required\">Сообщение:</span>
            <textarea class=\"form-control\" id=\"question-message\" name=\"question-message\" rows=\"6\" required></textarea>
        </label>

        <div class=\"form-submit-wrapper\">
            <button class=\"form-submit button\" type=\"submit\">Отправить</button>
        </div>
    </form>
</div>

<div class=\"popup popup-callback mfp-hide\" id=\"popup-thanks\">
    <div class=\"popup__title\">Спасибо за обращение</div>
</div>

<!-- Scripts -->

<script src=\"https://code.jquery.com/jquery-2.2.4.min.js\"></script>
<script src=\"{{ 'assets/js/jquery.magnific-popup.min.js'|theme }}\"></script>
<script src=\"{{ 'assets/js/script.js'|theme }}\"></script>
<script src=\"{{ 'assets/vendor/bootstrap.js'|theme }}\"></script>
<script src=\"{{ 'assets/javascript/app.js'|theme }}\"></script>
{% framework extras %}
{% scripts %}

<script>

    if(window.location.pathname=='/'){

        \$('body').addClass('homepage');
    }
</script>
<!-- Yandex.Metrika counter -->
<script type=\"text/javascript\">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter41686964 = new Ya.Metrika({
                    id: 41686964,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName(\"script\")[0],
                s = d.createElement(\"script\"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
        s.type = \"text/javascript\";
        s.async = true;
        s.src = \"https://mc.yandex.ru/metrika/watch.js\";

        if (w.opera == \"[object Opera]\") {
            d.addEventListener(\"DOMContentLoaded\", f, false);
        } else {
            f();
        }
    })(document, window, \"yandex_metrika_callbacks\");
</script>


<script>
    
    function ok (data){
        if (data.result=='ok'){
            \$('#popup-thanks').modal('show');
            \$.magnificPopup.open({
              items: {
                src: '#popup-thanks'
              },
              type: 'inline'

            }, 0);
        }
    }
</script>

<noscript>
    <div><img src=\"https://mc.yandex.ru/watch/41686964\" style=\"position:absolute; left:-9999px;\" alt=\"\"/></div>
</noscript>
<!-- /Yandex.Metrika counter -->


</body>
</html>", "D:\\OpenServer\\domains\\eco.local/themes/demo/layouts/default.htm", "");
    }
}
