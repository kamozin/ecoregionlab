<?php

/* D:\OpenServer\domains\eco.local/themes/demo/partials/top_nav.htm */
class __TwigTemplate_e6b7d6c309cc93d1e57ad9eca1052981837958736bbfaf862af374d6874af899 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<a class=\"main-nav__link\" href=\"/\">
    <span class=\"main-nav__text-link\">О нас</span>
</a>
<a class=\"main-nav__link\" href=\"/documents\">
    <span class=\"main-nav__text-link\">Документы</span>
</a>
<a class=\"main-nav__link\" href=\"/activities\">
    <span class=\"main-nav__text-link\">Виды <span>деятельности</span></span>
</a>
<a class=\"main-nav__link\" href=\"/clients\">
    <span class=\"main-nav__text-link\">Клиенты</span>
</a>
<a class=\"main-nav__link\" href=\"/partners\">
    <span class=\"main-nav__text-link\">Партнеры</span>
</a>
<a class=\"main-nav__link\" href=\"/links-to-federal\">
    <span class=\"main-nav__text-link\">Ссылки на <span>федеральные</span> <span>службы</span></span>
</a>
<a class=\"main-nav__link\" href=\"/contact\">
    <span class=\"main-nav__text-link\">Контакты</span>
</a>";
    }

    public function getTemplateName()
    {
        return "D:\\OpenServer\\domains\\eco.local/themes/demo/partials/top_nav.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<a class=\"main-nav__link\" href=\"/\">
    <span class=\"main-nav__text-link\">О нас</span>
</a>
<a class=\"main-nav__link\" href=\"/documents\">
    <span class=\"main-nav__text-link\">Документы</span>
</a>
<a class=\"main-nav__link\" href=\"/activities\">
    <span class=\"main-nav__text-link\">Виды <span>деятельности</span></span>
</a>
<a class=\"main-nav__link\" href=\"/clients\">
    <span class=\"main-nav__text-link\">Клиенты</span>
</a>
<a class=\"main-nav__link\" href=\"/partners\">
    <span class=\"main-nav__text-link\">Партнеры</span>
</a>
<a class=\"main-nav__link\" href=\"/links-to-federal\">
    <span class=\"main-nav__text-link\">Ссылки на <span>федеральные</span> <span>службы</span></span>
</a>
<a class=\"main-nav__link\" href=\"/contact\">
    <span class=\"main-nav__text-link\">Контакты</span>
</a>", "D:\\OpenServer\\domains\\eco.local/themes/demo/partials/top_nav.htm", "");
    }
}
