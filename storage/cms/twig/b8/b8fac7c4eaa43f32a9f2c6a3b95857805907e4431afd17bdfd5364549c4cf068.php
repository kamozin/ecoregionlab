<?php

/* D:\OpenServer\domains\eco.local/themes/demo/partials/site/header.htm */
class __TwigTemplate_b971e04d2b0557f95b8f2fb53cdbd07360997981235720daaa58b3b96fe6ba76 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"main-header\">
    <div class=\"main-header__top-panel\">
        <div class=\"main-header__inner container\">
            <a style=\"text-decoration: none\" href=\"/\" class=\"main-header-logo\">
                <div class=\"main-header-logo__image\">
                    <img src=\"";
        // line 6
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("/assets/img/logo.png");
        echo "\" alt=\"\">
                </div>
                <div class=\"main-header-logo__text\">
                    <div class=\"main-header-logo__name\">ЭКО РЕГИОН ЛАБ</div>
                    <h1 class=\"main-header-logo__description\">Лаборатория экологического мониторинга</h1>
                </div>
            </a>

            <div class=\"main-header-info\">
                <p class=\"main-header-address\">
                    г.Брянск, <br> ул. Бурова 12а
                </p>
                <div class=\"main-header-holder-boxes\">
                    <div class=\"main-header-phone\">
                        Телефон: <a href=\"tel:+79512561278\">+7 (4832) 40 00 39</a>
                    </div>
                    <a href=\"#popup-callback\" class=\"button button--fluid js-trigger-inline-popup\">Обратный звонок</a>
                    <a href=\"#popup-question\" class=\"button button--fluid js-trigger-inline-popup\">Задать вопрос</a>
                </div>
            </div>
        </div>
    </div>
    <div class=\"main-header__bottom-panel\">
        <div class=\"main-nav-wrapper\">
            <nav class=\"main-nav container\">
                ";
        // line 31
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('CMS')->partialFunction("top_nav"        , $context['__cms_partial_params']        );
        unset($context['__cms_partial_params']);
        // line 32
        echo "            </nav>
        </div>
    </div>
</header>";
    }

    public function getTemplateName()
    {
        return "D:\\OpenServer\\domains\\eco.local/themes/demo/partials/site/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 32,  54 => 31,  26 => 6,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header class=\"main-header\">
    <div class=\"main-header__top-panel\">
        <div class=\"main-header__inner container\">
            <a style=\"text-decoration: none\" href=\"/\" class=\"main-header-logo\">
                <div class=\"main-header-logo__image\">
                    <img src=\"{{ '/assets/img/logo.png'|theme }}\" alt=\"\">
                </div>
                <div class=\"main-header-logo__text\">
                    <div class=\"main-header-logo__name\">ЭКО РЕГИОН ЛАБ</div>
                    <h1 class=\"main-header-logo__description\">Лаборатория экологического мониторинга</h1>
                </div>
            </a>

            <div class=\"main-header-info\">
                <p class=\"main-header-address\">
                    г.Брянск, <br> ул. Бурова 12а
                </p>
                <div class=\"main-header-holder-boxes\">
                    <div class=\"main-header-phone\">
                        Телефон: <a href=\"tel:+79512561278\">+7 (4832) 40 00 39</a>
                    </div>
                    <a href=\"#popup-callback\" class=\"button button--fluid js-trigger-inline-popup\">Обратный звонок</a>
                    <a href=\"#popup-question\" class=\"button button--fluid js-trigger-inline-popup\">Задать вопрос</a>
                </div>
            </div>
        </div>
    </div>
    <div class=\"main-header__bottom-panel\">
        <div class=\"main-nav-wrapper\">
            <nav class=\"main-nav container\">
                {% partial \"top_nav\" %}
            </nav>
        </div>
    </div>
</header>", "D:\\OpenServer\\domains\\eco.local/themes/demo/partials/site/header.htm", "");
    }
}
