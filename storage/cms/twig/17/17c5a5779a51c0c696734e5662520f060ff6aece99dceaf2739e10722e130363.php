<?php

/* D:\OpenServer\domains\eco.local/themes/demo/pages/partnery.htm */
class __TwigTemplate_317276b22786d9003684e5337588910a332cdc18a6fef1645f1b434524b38199 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"content-area\">
          <div class=\"page__header container\">
            <h1 class=\"page__title\">Партнёры</h1>
          </div>

          <div class=\"page__content container\">
            <div class=\"decor-boxes\">

";
        // line 9
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("partnersView"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 10
        echo "            </div>
          </div>
        </div>";
    }

    public function getTemplateName()
    {
        return "D:\\OpenServer\\domains\\eco.local/themes/demo/pages/partnery.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 10,  29 => 9,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"content-area\">
          <div class=\"page__header container\">
            <h1 class=\"page__title\">Партнёры</h1>
          </div>

          <div class=\"page__content container\">
            <div class=\"decor-boxes\">

{% component 'partnersView' %}
            </div>
          </div>
        </div>", "D:\\OpenServer\\domains\\eco.local/themes/demo/pages/partnery.htm", "");
    }
}
