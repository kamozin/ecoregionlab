$(function() {
  /*==============================
  =            Popups            =
  ==============================*/
  
  var $triggerInlinePopup = $('.js-trigger-inline-popup');
  var $triggerImagePopup = $('.js-trigger-image-popup');
  var $triggerGalleryPopup = $('.js-trigger-gallery-popup');



  if ($triggerInlinePopup.length) {
    $triggerInlinePopup.magnificPopup({
      mainClass: 'popup-fade',
      removalDelay: 300
    });
  }

  if ($triggerImagePopup) {
    $triggerImagePopup.magnificPopup({
      type: 'image',
      image: {
        verticalFit: false
      },
      mainClass: 'popup-fade',
      removalDelay: 300
    });
  }

  if ($triggerGalleryPopup) {
    $triggerGalleryPopup.magnificPopup({
      delegate: 'a',
      type: 'image',
      image: {
        verticalFit: false
      },
      mainClass: 'popup-fade',
      removalDelay: 300,
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
      },
    });
  }
  
  /*=====  End of Popups  ======*/
  
});
